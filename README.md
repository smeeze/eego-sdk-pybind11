# eego-SDK python wrapper

Python wrapper for the eego-SDK, using pybind11. The wrapper tries to follow
the eego-SDK API as much as possible. Please refer to the official eego-SDK
docs how to use the API.

## Requirements
* C++ compiler that supports C++11
* cmake 3.11
* git
* python
* eego-SDK

## Supported platforms
* Windows (32 and 64 bit)
* Linux (32 and 64 bit)

## Setup
Create a user.cmake file in the root of this project. Add a line pointing
to the eego-SDK zip file, like so:
```
set(EEGO_SDK_ZIP /path/to/download/eego-sdk-1.3.19.40453.zip)
```

## Build
Build using standard cmake. i.e. create build directory and
from there, call cmake:
```
cmake -DCMAKE_BUILD_TYPE=Release /path/to/this_project && cmake --build . --config Release
```

## Running
Make sure Python can find the created module and the eego-SDK library, i.e.
by adjusting the PYTHONPATH and/or the library search path.

Take a look at the stream.py example how to interact with the factory,
amplifier and stream objects.

The provided stream.py test program loops over each amplifier, shows the
impedances, then records 10 second of EEG into a text file. Afterwards,
it puts the amplifier in a dictionary based on its type.
At the end, the dictionary is checked, and if there are 2 or 4 amplifiers
of each type, it will run a cascaded test on these 2 or 4 amplifiers, provided
that cascading is supported in the SDK.

## Trouble-shooting

### ImportError: DLL load failed while importing eego_sdk: The specified module could not be found.

- Make sure Python can find the eego-SDK library, i.e. by adjusting the PYTHONPATH and/or the library search path.

- Make sure that cmake uses the same Python version as you use for your project (in particular if you have
multiple versions of Python2 or multiple versions of Python3). The version can be specified in two ways:
	- Specify in `python2/CMakeLists.txt` and/or `python3/CMakeLists.txt`. E.g., if you want to use Python 3.9,
	this can be done by adding the following as a first line in the respective CMakeLists.txt:
	```
	find_package(Python3 3.9 EXACT COMPONENTS Development)
	```
	
	- Specify the Python library in the cmake command. E.g., if you want to use Python 3.9:
	```
	cmake -DCMAKE_BUILD_TYPE=Release -DPython3_LIBRARY="C:\path\to\Python39\libs\python39.lib" /path/to/this_project && cmake --build . --config Release
	```